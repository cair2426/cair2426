package cair2426.note.controller;

import cair2426.note.main.ClasaException;
import cair2426.note.model.Corigent;
import cair2426.note.model.Elev;
import cair2426.note.model.Medie;
import cair2426.note.model.Nota;
import cair2426.note.repository.*;

import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class NoteController {
    private NoteRepository note;
    private ClasaRepository clasa;
    private EleviRepository elevi;

    public NoteController() {
        note = new NoteRepositoryMock();
        clasa = new ClasaRepositoryMock();
        elevi = new EleviRepositoryMock();
    }

    public Nota readNota() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduceti numarul matricol: ");
        double nrMatricol = scanner.nextDouble();
        System.out.print("Introduceti materia: ");
        String materie = scanner.next();
        System.out.print("Introduceti nota: ");
        double nota = scanner.nextDouble();
        return new Nota(nrMatricol, materie, nota);
    }

    public void addNota(Nota nota) throws ClasaException {
        note.addNota(nota);
    }

    public void addElev(Elev elev) {
        elevi.addElev(elev);
    }

    public void creeazaClasa(List<Elev> elevi, List<Nota> note) {
        clasa.creazaClasa(elevi, note);
    }

    public List<Medie> calculeazaMedii() throws ClasaException {
        List<Medie> rezultate = clasa.calculeazaMedii();
        rezultate.sort((o1, o2) -> (int) Math.round(o2.getMedie() - o1.getMedie()));
        return rezultate;

    }

    public List<Nota> getNote() {
        return note.getNote();
    }

    public List<Elev> getElevi() {
        return elevi.getElevi();
    }

    public HashMap<Elev, HashMap<String, List<Double>>> getClasa() {
        return clasa.getClasa();
    }

    public void afiseazaClasa() {
        clasa.afiseazaClasa();
    }

    public void readElevi(String fisier) {
        elevi.readElevi(fisier);
    }

    public void readNote(String fisier) {
        note.readNote(fisier);
    }

    public List<Corigent> getCorigenti() {
        return clasa.getCorigenti();
    }
}
