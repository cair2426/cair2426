package cair2426.note.repository;

import cair2426.note.main.ClasaException;
import cair2426.note.main.CorigentiException;
import cair2426.note.model.Corigent;
import cair2426.note.model.Elev;
import cair2426.note.model.Medie;
import cair2426.note.model.Nota;
import cair2426.note.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class ClasaRepositoryMock implements ClasaRepository {

    private HashMap<Elev, HashMap<String, List<Double>>> clasa;

    public ClasaRepositoryMock() {
        clasa = new HashMap<Elev, HashMap<String, List<Double>>>();
    }

    @Override
    public void creazaClasa(List<Elev> elevi, List<Nota> note) {
        // TODO Auto-generated method stub
        List<String> materii = new LinkedList<String>();
        for (Nota nota : note) {
            if (!materii.contains(nota.getMaterie()))
                materii.add(nota.getMaterie());
        }
        for (Elev elev : elevi) {
            HashMap<String, List<Double>> situatie = new HashMap<String, List<Double>>();
            for (String materie : materii) {
                List<Double> noteMaterie = new LinkedList<Double>();
                for (Nota nota : note)
                    if (nota.getMaterie().equals(materie) && nota.getNrmatricol() == elev.getNrmatricol())
                        noteMaterie.add(nota.getNota());
                situatie.put(materie, noteMaterie);
            }
            clasa.put(elev, situatie);
        }

    }

    @Override
    public HashMap<Elev, HashMap<String, List<Double>>> getClasa() {
        // TODO Auto-generated method stub
        return clasa;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public List<Medie> calculeazaMedii() throws ClasaException {
        // TODO Auto-generated method stub
        List<Medie> medii = new LinkedList<Medie>();
        if (clasa.size() > 0) {
            int ci = 0;
            List<Elev> elevi = getListaElevi();
            while (ci < elevi.size()) {
                Elev elev = elevi.get(ci);
                Medie medie = new Medie();
                medie.setElev(elev);
                int nrMaterii = 0;
                double sumaMedii = 0;
                double medieElev = 0;
                int cj = 0;
                List<String> materii = new ArrayList<>(clasa.get(elev).keySet());
                while (cj < materii.size()) {
                    String materie = materii.get(cj);
                    nrMaterii++;
                    List<Double> noteElev = clasa.get(elev).get(materie);
                    int nrNote = noteElev.size();
                    int i = 0;
                    double suma = 0;
                    if (nrNote >= 0) {
                        while (i < nrNote) {
                            double nota = noteElev.get(i);
                            suma += nota;
                            i++;
                        }
                        sumaMedii = sumaMedii + suma / i;
                    }
                    cj++;
                }
                medieElev = sumaMedii / nrMaterii;
                medie.setMedie(medieElev);
                medii.add(medie);
                ci++;
            }
        } else
            throw new ClasaException(Constants.emptyRepository);
        return medii;
    }

    public List<Elev> getListaElevi() {
        return new ArrayList<>(clasa.keySet());
    }

    public void afiseazaClasa() {
        for (Elev elev : clasa.keySet()) {
            System.out.println(elev);
            for (String materie : clasa.get(elev).keySet()) {
                System.out.println(materie);
                for (double nota : clasa.get(elev).get(materie))
                    System.out.print(nota + " ");
            }
        }
    }

    @Override
    public List<Corigent> getCorigenti() {
        List<Corigent> corigenti = new ArrayList<Corigent>();
        if (clasa.size() == 0) {
            throw new CorigentiException("Nu se pot lua corigenti dintr-o clasa inexistenta!");
        }
        if (clasa.size() >= 0) {
            for (Elev elev : clasa.keySet()) {
                Corigent corigent = new Corigent(elev.getNume(), 0);
                for (String materie : clasa.get(elev).keySet()) {
                    List<Double> noteElev = clasa.get(elev).get(materie);
                    int nrNote = noteElev.size();
                    int i = 0;
                    double suma = 0;
                    if (nrNote >= 0) {
                        while (i < nrNote) {
                            double nota = noteElev.get(i);
                            suma += nota;
                            i++;
                        }
                        double media = suma / i;
                        if (media < 4.5)
                            corigent.setNrMaterii(corigent.getNrMaterii() + 1);
                    }
                }
                if (corigent.getNrMaterii() > 0) {
                    int i = 0;
                    while (i < corigenti.size() && corigenti.get(i).getNrMaterii() > corigent.getNrMaterii())
                        i++;
                    if (i != corigenti.size() && corigenti.get(i).getNrMaterii() == corigent.getNrMaterii()) {
                        while (i < corigenti.size() && corigenti.get(i).getNrMaterii() == corigent.getNrMaterii() && corigenti.get(i).getNumeElev().compareTo(corigent.getNumeElev()) < 0)
                            i++;
                        corigenti.add(i, corigent);
                    } else
                        corigenti.add(i, corigent);
                }
            }
        }
        return corigenti;
    }
}
