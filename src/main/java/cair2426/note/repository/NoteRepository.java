package cair2426.note.repository;

import cair2426.note.main.ClasaException;
import cair2426.note.model.Nota;

import java.util.List;

public interface NoteRepository {
	
	public void addNota(Nota nota) throws ClasaException;
	public List<Nota> getNote(); 
	public void readNote(String fisier);
	
}
