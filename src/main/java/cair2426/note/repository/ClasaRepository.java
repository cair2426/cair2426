package cair2426.note.repository;

import cair2426.note.main.ClasaException;
import cair2426.note.model.Corigent;
import cair2426.note.model.Elev;
import cair2426.note.model.Medie;
import cair2426.note.model.Nota;

import java.util.HashMap;
import java.util.List;

public interface ClasaRepository {
	
	public void creazaClasa(List<Elev> elevi, List<Nota> note);
	public HashMap<Elev, HashMap<String, List<Double>>> getClasa();
	public List<Medie> calculeazaMedii() throws ClasaException;
	public void afiseazaClasa();
	public List<Corigent> getCorigenti();
}
