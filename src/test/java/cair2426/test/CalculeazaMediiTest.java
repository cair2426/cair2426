package cair2426.test;

import cair2426.note.controller.NoteController;
import cair2426.note.main.ClasaException;
import cair2426.note.model.Elev;
import cair2426.note.model.Medie;
import cair2426.note.model.Nota;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static java.lang.Double.NaN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CalculeazaMediiTest {

    private NoteController ctrl;

    @Before
    public void init() {
        ctrl = new NoteController();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void test0() throws ClasaException {
        Elev e1 = new Elev(1, "Elev1");
        Elev e2 = new Elev(2, "Elev2");
        ctrl.addElev(e1);
        ctrl.addElev(e2);
        Nota n1 = new Nota(1, "Materie1", 10);
        Nota n2 = new Nota(1, "Materie1", 7);
        Nota n3 = new Nota(1, "Materie2", 10);
        Nota n4 = new Nota(1, "Materie2", 10);
        Nota n5 = new Nota(2, "Materie2", 4);
        Nota n6 = new Nota(2, "Materie2", 3);
        Nota n7 = new Nota(2, "Materie2", 6);
        Nota n8 = new Nota(2, "Materie1", 7);
        ctrl.addNota(n1);
        ctrl.addNota(n2);
        ctrl.addNota(n3);
        ctrl.addNota(n4);
        ctrl.addNota(n5);
        ctrl.addNota(n6);
        ctrl.addNota(n7);
        ctrl.addNota(n8);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(rezultate.size(), 2);
        assertEquals(9.25, rezultate.get(0).getMedie(), 0.01);
        assertEquals(5.66, rezultate.get(1).getMedie(), 0.01);
    }

    @Test
    public void test1() throws ClasaException {
        Elev e1 = new Elev(100, "elev1");
        Elev e2 = new Elev(101, "elev2");
        ctrl.addElev(e1);
        ctrl.addElev(e2);
        Nota n1 = new Nota(100, "Matematica", 10);
        Nota n2 = new Nota(100, "Matematica", 10);
        Nota n3 = new Nota(100, "Informatica", 10);
        Nota n4 = new Nota(100, "Informatica", 10);
        Nota n5 = new Nota(101, "Matematica", 8);
        Nota n6 = new Nota(101, "Matematica", 8);
        Nota n7 = new Nota(101, "Informatica", 9);
        Nota n8 = new Nota(101, "Informatica", 9);
        ctrl.addNota(n1);
        ctrl.addNota(n2);
        ctrl.addNota(n3);
        ctrl.addNota(n4);
        ctrl.addNota(n5);
        ctrl.addNota(n6);
        ctrl.addNota(n7);
        ctrl.addNota(n8);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        //ctrl.afiseazaClasa();
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(rezultate.size(), 2);
        assertTrue((rezultate.get(0).getMedie() == 10.0 && rezultate.get(1).getMedie() == 8.5) ||
                (rezultate.get(0).getMedie() == 8.5 && rezultate.get(1).getMedie() == 10.0));
    }

    @Test(expected = ClasaException.class)
    public void test2() throws ClasaException {
        ctrl.calculeazaMedii();
    }

    @Test
    public void test3() throws ClasaException {
        Elev e1 = new Elev(100, "elev1");
        ctrl.addElev(e1);
        Nota n1 = new Nota(100, "Matematica", 10);
        Nota n2 = new Nota(100, "Matematica", 10);
        Nota n3 = new Nota(100, "Informatica", 10);
        Nota n4 = new Nota(100, "Informatica", 10);
        ctrl.addNota(n1);
        ctrl.addNota(n2);
        ctrl.addNota(n3);
        ctrl.addNota(n4);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        //ctrl.afiseazaClasa();
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(rezultate.size(), 1);
        assertEquals(10.0, rezultate.get(0).getMedie(), 0.01);
    }

    @Test
    public void test4() throws ClasaException {
        Elev e1 = new Elev(102, "elev3");
        ctrl.addElev(e1);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(rezultate.size(), 1);
        assertEquals(NaN, rezultate.get(0).getMedie(), 0.01);
    }

    @Test
    public void test6() throws ClasaException {
        Elev e1 = new Elev(105, "elev6");
        ctrl.addElev(e1);
        Nota n1 = new Nota(105, "Matematica", 8);
        ctrl.addNota(n1);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        //ctrl.afiseazaClasa();
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(rezultate.size(), 1);
        assertEquals(8, rezultate.get(0).getMedie(), 0.01);
    }
}
