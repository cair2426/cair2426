package cair2426.test;

import cair2426.note.controller.NoteController;
import cair2426.note.main.ClasaException;
import cair2426.note.model.Corigent;
import cair2426.note.model.Elev;
import cair2426.note.model.Medie;
import cair2426.note.model.Nota;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class BigBangIntegrationTest {

    @Test
    public void testareUnitaraA() throws ClasaException {
        NoteController ctrl;
        ctrl = new NoteController();
        Nota nota = new Nota(1, "Desen", 10);
        ctrl.addNota(nota);
        assertEquals(1, ctrl.getNote().size());
    }

    @Test
    public void testareUnitaraB() throws ClasaException {
        NoteController ctrl;
        ctrl = new NoteController();
        Elev e1 = new Elev(100, "elev1");
        Elev e2 = new Elev(101, "elev2");
        ctrl.addElev(e1);
        ctrl.addElev(e2);
        Nota n1 = new Nota(100, "Matematica", 10);
        Nota n2 = new Nota(100, "Matematica", 10);
        Nota n3 = new Nota(100, "Informatica", 10);
        Nota n4 = new Nota(100, "Informatica", 10);
        Nota n5 = new Nota(101, "Matematica", 8);
        Nota n6 = new Nota(101, "Matematica", 8);
        Nota n7 = new Nota(101, "Informatica", 9);
        Nota n8 = new Nota(101, "Informatica", 9);
        ctrl.addNota(n1);
        ctrl.addNota(n2);
        ctrl.addNota(n3);
        ctrl.addNota(n4);
        ctrl.addNota(n5);
        ctrl.addNota(n6);
        ctrl.addNota(n7);
        ctrl.addNota(n8);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        //ctrl.afiseazaClasa();
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(rezultate.size(), 2);
        assertEquals(10.0, rezultate.get(0).getMedie(), 0.01);
        assertEquals(8.5, rezultate.get(1).getMedie(), 0.01);
    }

    @Test
    public void testareUnitaraC() throws ClasaException {
        NoteController ctrl;
        ctrl = new NoteController();
        Elev e1 = new Elev(1, "Ana");
        Elev e2 = new Elev(2, "Gigel");
        Elev e3 = new Elev(3, "Dorel");
        ctrl.addElev(e1);
        ctrl.addElev(e2);
        ctrl.addElev(e3);
        Nota n1 = new Nota(1, "Materie1", 2);
        Nota n2 = new Nota(1, "Materie1", 6);
        Nota n3 = new Nota(1, "Materie2", 3);
        Nota n4 = new Nota(1, "Materie2", 4);
        Nota n5 = new Nota(2, "Materie2", 4);
        Nota n6 = new Nota(2, "Materie2", 4);
        Nota n7 = new Nota(2, "Materie1", 6);
        Nota n8 = new Nota(2, "Materie1", 7);
        Nota n9 = new Nota(3, "Materie3", 3);
        Nota n10 = new Nota(3, "Materie3", 2);
        ctrl.addNota(n1);
        ctrl.addNota(n2);
        ctrl.addNota(n3);
        ctrl.addNota(n4);
        ctrl.addNota(n5);
        ctrl.addNota(n6);
        ctrl.addNota(n7);
        ctrl.addNota(n8);
        ctrl.addNota(n9);
        ctrl.addNota(n10);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Corigent> corigenti = ctrl.getCorigenti();
        assertEquals(3, corigenti.size());
        assertEquals("Ana", corigenti.get(0).getNumeElev());
        assertEquals(2, corigenti.get(0).getNrMaterii());
        assertEquals("Dorel", corigenti.get(1).getNumeElev());
        assertEquals(1, corigenti.get(1).getNrMaterii());
        assertEquals("Gigel", corigenti.get(2).getNumeElev());
        assertEquals(1, corigenti.get(2).getNrMaterii());
    }

    @Test
    public void testIntegrare() throws ClasaException {
        //P->A->B->C
        NoteController ctrl;
        ctrl = new NoteController();
        Elev e1 = new Elev(1, "Elev1");
        ctrl.addElev(e1);
        Nota nota = new Nota(1, "Desena", 10);
        ctrl.addNota(nota);
        assertEquals(1, ctrl.getNote().size());
        assertEquals(10, ctrl.getNote().get(0).getNota(), 0.01);

        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(1, rezultate.size());
        assertEquals(10, rezultate.get(0).getMedie(), 0.01);

        List<Corigent> corigenti = ctrl.getCorigenti();
        assertEquals(corigenti.size(), 0);
    }
}
