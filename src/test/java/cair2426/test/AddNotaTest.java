package cair2426.test;

import cair2426.note.controller.NoteController;
import cair2426.note.main.ClasaException;
import cair2426.note.model.Nota;
import cair2426.note.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class AddNotaTest {
	
	private NoteController ctrl;
	
	@Before
	public void init(){
		ctrl = new NoteController();
	}
	
	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

	@Test
	public void test1() throws ClasaException {
		Nota nota = new Nota(1, "Desen", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test2() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		Nota nota = new Nota(10.1, "Istorie", 5);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test3() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		Nota nota = new Nota(0, "Istorie", 5);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test4() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidMateria);
		Nota nota = new Nota(1, "Isto", 5);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test5() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		Nota nota = new Nota(1, "Istorie", 5.002);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test6() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		Nota nota = new Nota(1, "Istorie", 11);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test7() throws ClasaException {
		Nota nota = new Nota(2, "Istorie", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test8() throws ClasaException {
		Nota nota = new Nota(1000, "Istorie", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test9() throws ClasaException {
		Nota nota = new Nota(999, "Istorie", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test10() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		Nota nota = new Nota(1001, "Istorie", 5);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test11() throws ClasaException {
		Nota nota = new Nota(1000, "Desena", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test12() throws ClasaException {
		Nota nota = new Nota(1000, "DesenDesenDesenDesen", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test13() throws ClasaException {
		Nota nota = new Nota(1000, "DesenDesenDesenDese", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	
	@Test
	public void test14() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidMateria);
		Nota nota = new Nota(1, "DesenDesenDesenDesenD", 5);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test15() throws ClasaException {
		Nota nota = new Nota(1000, "DesenDesenDesenDese", 1);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test16() throws ClasaException {
		Nota nota = new Nota(1000, "DesenDesenDesenDese", 2);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test17() throws ClasaException {
		Nota nota = new Nota(1000, "DesenDesenDesenDese", 9);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
	
	@Test
	public void test18() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		Nota nota = new Nota(1, "Istorie", 0);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test19() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidMateria);
		Nota nota = new Nota(1, "", 5);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test20() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidMateria);
		Nota nota = new Nota(1, "a", 5);
		ctrl.addNota(nota);
	}
	
	@Test
	public void test21() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		Nota nota = new Nota(1, "Istorie", -2);
		ctrl.addNota(nota);
	}
	
	@Test
	public void tes22() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		Nota nota = new Nota(-1001, "Istorie", 5);
		ctrl.addNota(nota);
	}

	//my tests
	@Test
	public void tes23() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		Nota nota = new Nota(1001.5, "Matematica", 4);
		ctrl.addNota(nota);
	}

	@Test
	public void tes24() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		Nota nota = new Nota(2, "Desen", -3.5);
		ctrl.addNota(nota);
	}

	@Test
	public void tes25() throws ClasaException {
		Nota nota = new Nota(999, "Istorie", 9);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void tes26() throws ClasaException {
		Nota nota = new Nota(1000, "Informatica", 10);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void tes27() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		Nota nota = new Nota(1001, "Geografie", 5);
		ctrl.addNota(nota);
	}

	@Test
	public void tes28() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		Nota nota = new Nota(2, "Geografie", 0);
		ctrl.addNota(nota);
	}

	@Test
	public void tes29() throws ClasaException {
		Nota nota = new Nota(3, "Istorie", 1);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}

	@Test
	public void test30() throws ClasaException {
		Nota nota = new Nota(1, "Desen", 1);
		ctrl.addNota(nota);
		assertEquals(1, ctrl.getNote().size());
	}
}
